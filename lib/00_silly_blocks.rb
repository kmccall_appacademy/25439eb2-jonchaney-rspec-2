def reverser(&block)
  block.call.split.map do |el|
    reverse_word(el)
  end.join(" ")
end

def reverse_word(word)
  word.reverse
end

def adder(num=1, &block)
  block.call + num
end

def repeater(num=1,&block)
  num.times { block.call }
end
