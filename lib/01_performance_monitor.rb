require 'time'

def measure(n=1, &block)
  run_times = []
  if n > 1
    n.times { run_times << block.call }
      if run_times[0].class == Time
        get_average(run_times)
      end
  else
    run_time(&block)
  end
end

def run_time(&block)
  t = Time.now
  yield
  Time.now - t
end

def get_average(run_times)
  run_times.unshift(@eleven_am)
  sum = []
  i = 0
  while i < run_times.length-1
    sum << run_times[i+1] - run_times[i]
    i+=1
  end
  sum.reduce(:+) / sum.length
end
